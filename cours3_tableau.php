<?php 

/* ******************** */
/* ***** Tableau ****** */
/* ******************** */

// indicé

$myArray = ['Pomme', 'poire', "patate"];
$myArray[] = 'banane';
echo "<pre>";
echo $myArray . "<br>";
var_dump($myArray);

echo $myArray[1];

$myArrayBis = array('Poireau', 'carotte', 'navet');

echo array_pop($myArray); // supprime et retourne la derniere valeur du tableau

echo '<hr>';

// Associatif
$perso1 = [
    'name' => 'Zorglub',
    'age' => 250,
    'str' => 40,
];

echo "Mon perso s'appele " . $perso1['name'];

$perso2 = [
    'name' => 'Gandalf',
    'age' => 900,
    'str' => 9000
];

$persos = [
    'joueur1' => $perso1,
    'joueur2' => $perso2
];

echo $persos['joueur2']['age'];

 // phpinfo(); // montre les variables d'environement 

