<?php

class Etudiant {
    private $id;
    private $nom;
    private $groupe;

    public function apprendre(){
        return "J'apprend";
    }

    public function __construct($id, $name) {
        $this->id = $id;
        $this->nom = $name;
        
    }

    /*
        ACCESSEUR/MUTATEUR/Getter Setter
    */
    public function getNom(){
        return strtoupper($this->nom);
    }

    public function setNom($name){
        $this->nom = $name;
    }

  

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}