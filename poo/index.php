<?php

require_once "Matiere.php";
require_once "Etudiant.php";
require_once "Groupe.php";

// creation d'une nouvelle instance de la classe Matiere
$matiere = new Matiere();

// en ecriture, j'ai mis les valeurs dans les propriétés
$matiere->nom = "Combat contre les forces du mal";
$matiere->contenu = "Blablabla";
$matiere->prof = "ROGUE";

// nouvelle instance
$matiere2 = new Matiere();

$matiere2->nom = "Potion";
$matiere2->contenu = "Blablabla blabla";
$matiere2->prof = "Pommefresh";


// en lecture
echo $matiere->nom; // affiche PHP
echo $matiere->evaluer();

/*
    Etudiant
*/
// nouvelle instance d'étudiant
$etu1 = new Etudiant(1, "Harry Potter");
$etu2 = new Etudiant(2, "Hermione Granger");
// je met dans le nom le string fourni
// $etu1->setNom("Ron");

// je recupere la valeur de la propriété nom
echo $etu1->getNom();

/*
    GROUPE
*/
$griffondor = new Groupe("Griffondor", "Mcgonagall");
$griffondor->ajoutEtudiant($etu1);
$griffondor->ajoutEtudiant($etu2);


echo "<hr><pre>";
var_dump($griffondor->getEtudiants());
// echo "Je m'appele " . $griffondor->getEtudiants()[0]->getNom() . " et je suis dans la maison " . $griffondor->getNom() ;
if ($griffondor->getEtudiantById($etu2->getNom())) {
    echo "Je m'appele " . $griffondor->getEtudiantById(4)->getNom() . " et je suis dans la maison " . $griffondor->getNom();
}

/*
    exemple avec un operateur ternaire

*/
// je verifie qu'il me retourne quelque chose. Avant le ?; la condition à vérifier
// après le ? le resultat à afficher si c'est OK
// après le : resultat si KO
echo "Je m'appele " . ($griffondor->getEtudiantById(4) ?  $griffondor->getEtudiantById(4)->getNom() : 'N\' pas de nom') . " et je suis dans la maison " . $griffondor->getNom();
//*/