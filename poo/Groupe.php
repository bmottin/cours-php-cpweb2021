<?php

class Groupe
{

    private $nom;
    private $etudiants;
    private $referent;
    private $matieres;

    public function __construct($name, $ref)
    {
        $this->nom = $name;
        $this->referent = $ref;

        $this->etudiants = array();
        $this->matieres = array();
    }

    /*
        ACCESSEUR
    */
    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($name)
    {
        $this->nom = $name;
    }

    public function ajoutEtudiant($etudiant)
    {
        array_push($this->etudiants, $etudiant);
    }

    public function getEtudiants()
    {
        return $this->etudiants;
    }

    public function getEtudiantById($id)
    {
        $res = false;
        foreach ($this->etudiants as $key => $etudiant) {

            if ($etudiant->getId() == $id) {
                $res = $etudiant;
                break;
            }
        }

        return $res;
    }
}
