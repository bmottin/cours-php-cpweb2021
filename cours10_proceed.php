<?php
echo "<pre>";
var_dump($_FILES);
$file = $_FILES['image'];
$error = $file['error'];
$is_valid_error = false;
$is_valid_type = false;

// gestion des errors
switch ($error) {
    case '0':
        $is_valid_error = true;
        echo "echo OK";
        break;
    case '1':
        echo "Fichier trop gros côté serveur";
        break;
    case '2':
        echo "Fichier trop gros côté navigateur";
        break;
    case '3':
        break;
    case '4':
        break;
    case '6':
        break;
    default:
        # code...
        break;
}

$unwanted_array = array(
    'Š' => 'S', 'š' => 's', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
    'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U',
    'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c',
    'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o',
    'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y',
    ' ' => '_'
);
$filename = strtr($file['name'], $unwanted_array);


// gestion des types
if(in_array(mime_content_type($file['tmp_name']), ['image/jpeg', '.pdf'])) {
    echo "bon type";
    $is_valid_type = true;
} else {
    echo "pas le bon type";
}

// Controler les erreurs
if($is_valid_error && $is_valid_type) {
    // si pas d'erreur on déplace le fichier
    move_uploaded_file($file['tmp_name'], './' . $filename);
} else {
    echo "une erreur est survenue";
    // sinon on affiche une erreur suivant le type d'erreur
    if(!$is_valid_type) {
        echo "Pas le bon type";
    } 
    
    if (!$is_valid_error) {
        echo "Erreur upload";
    }
}

