<?php
// GESTION DU FORMULAIRE
if(isset($_GET['word'])) {
    $mot = $_GET['word'];
} else {
    $mot = 'Bonjour';
}


?>


<?PHP

/*
# Consigne decrire les étapes pour inverser les lettre d'un mot fourni

var mot <- mot a inversé
var resultat
---------------------------------------------------------------
1ere réponse
# je déclare un tableau qui contiendra toutes les lettres du mot
var tableauDeLettre // tab['b','o', 'n', 'j', 'o', 'u', 'r']

pour chaque lettre
    je prend la derniere lettre du tableau // 1er tour r | 2eme u
    je la stocke dans var resultat // 1er res = r | 2eme res = ru
fin pour

j'affiche le resultat

//*/
echo "<pre>";
// le mot en dur
// DEBUG, retirer mot en dur
// $mot = 'bonjour';
// receptable du resultat
$resultat = "";

// je découpe le mot en un tableau
$tableauDeLettre = str_split($mot);
var_dump($tableauDeLettre);
// je recupeter la taille du tableau pour définir le nombre de tour de boucle
$limit = count($tableauDeLettre);

// pour chaque elm du tableau je retire la derniere lettre 
// et la concatene dans mon resultat
for ($i=0; $i < $limit; $i++) { 
    $resultat .= array_pop($tableauDeLettre);
}

// j'affiche le resultat
echo "Le mot " . $mot . " a l'envers donne " . $resultat;

echo "<hr>";

/*
----------------------------------------------------------------

2eme alternative

var mot <- mot a inversé
var resultat

var limit <- recupere la taille du tableau
tant que la limit n'est pas attenite, en partant de la fin du mot
    je pointe à l'index du tour
    res = res + le lettre pointé 
    je retire 1 à l'index
fin tant que

j'affiche le resultat
 */
// FIXME supprimer le mot en dur DEBUG
//  $mot = "Bonjour";
 $res = "";
 // je recuperer la taille du mot
 // -1 car cela représentera l'index du str
 $cpt = strlen($mot)-1;

 // pour je décremente l'index
 for ($i=$cpt; $i >= 0; $i--) { 
     $res .= $mot[$i];
 }

 echo "Le mot " . $mot . " a l'envers donne " . $res;

 ?>

 <form action="#" method="get">
    <input type="text" name="word">
    <input type="submit" value="inversé">
 </form>