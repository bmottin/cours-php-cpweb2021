<?php

// les 3 valeurs (stockées dans un tableau)
// reponse User
// reponse cpu
// --------------

// traitement
// 1-recupere la réponse utilisateur
// 2-recupere réponse cpu
//  je tire un chiffre aléatoire 
//  et je recupere la valeur à la position tirée dans le tableau
// 3- comparaison
//  égalité 
//  user == pierre & cpu == ciseau 
//  user == ciseau & cpu == feuille
//  user == feuille & cpu == pierre
//  sinon ecrit "perdu"

// ---------------------------------------
// -----------Realisation-----------------
// ---------------------------------------
//              0         1         2
$reponse = ['pierre', 'feuille', 'ciseau']; // reponse possible
$chiffreAleatoire = rand(0, 2); // retourne un chiffre compris entre 0 et 2
$reponseCpu = $reponse[$chiffreAleatoire]; // recuperer la valeur dans le tableau via sa position

// echo "<pre>";
// echo "reponse CPU<br>";
// var_dump($reponseCpu); // FIXME
// echo "reponse Utilisateur<br>";
// var_dump($_GET['reponse_user']); // FIXME

// je vérifie que la clé existe dans mon tableau GET
// donc le jeu à commencé
if (isset($_POST['reponse_user'])) {



    // booleen pour savoir si le joueur à gagné
    $hasWin = false;
    // booleen pour savoir si c'est égalité
    $isDraw = false;

    $reponseUser = $_POST['reponse_user'];
    
    echo "Votre réponse <strong>" . showIcon($reponseUser) . "</strong> VS. réponse CPU <strong>" . showIcon($reponseCpu) . "</strong><br>";

    // comparer les réponses
    if ($reponseUser == $reponseCpu) {
        $isDraw = true;
    } else if ($reponseUser == "pierre" && $reponseCpu == "ciseau") {
        $hasWin = true; // donc il a gagné
    } else if ($reponseUser == "ciseau" && $reponseCpu == "feuille") {
        $hasWin = true;
    } else if ($reponseUser == "feuille" && $reponseCpu == "pierre") {
        $hasWin = true;
    }

    //if ($hasWin == true) {
    if ($hasWin) {
        echo "Gagné !!";
    } else if ($isDraw) {
        echo "Égalité";
    } else {
        echo "C'est perdu !!!";
    }
}

function showIcon($reponse) {
    switch ($reponse) {
        case 'pierre':
            return "✊🏻";
            break;
        case 'feuille':
            return "✋🏻";
            break;
        case 'ciseau':
            return "✌🏻";
            break;
    }
}


// /\ PHP
?>
<!-- \/ HTML -->

<form action="#" method="post">

    <select name="reponse_user">
        <option value="pierre"> Pierre </option>
        <option value="feuille"> Feuille </option>
        <option value="ciseau"> Ciseau </option>
    </select>

    <!-- <label for="">Pierre
        <input type="radio" name="reponse_user" value="pierre" id="">
    </label>
    <label for="">Feuille
        <input type="radio" name="reponse_user" value="feuille" id="">
    </label>
    <label for="">Ciseau
        <input type="radio" name="reponse_user" value="ciseau" id="">
    </label> -->

    <!--     
    <button type="submit" name="reponse_user" value="pierre">✊🏻</button>
    <button type="submit" name="reponse_user" value="feuille">✋🏻</button>
    <button type="submit" name="reponse_user" value="ciseau">✌🏻</button> -->


    <input type="submit" value="Jouer !">

</form>