<?php

/* ********************* */
/* *******Boucle******** */
/* ********************* */

// for

// 1er param le départ
// 2eme la limite
// 3eme le pas, de combien on avance à chaque tour
for ($i=0; $i < 10; $i++) { 
    echo "<h2>Je compte " . $i . "</h2>";
}

// attention à la boucle infini
$a = 0;
while ($a <= 10) {
    echo "j'attends 10 et j'en suis à " . $a . "<br>";
    $a++;
}

// s'executre au moins une fois
do {
    echo "j'attends 10 et j'en suis à " . $a . "<br>";
    $a++;
} while ($a <= 10);


echo "<hr>";
// foreach
$myArray = ['Pomme', "Poire", 'Banane', 'kiwi'];

foreach ($myArray as $key => $fruit) {
    echo 'Ajouter dans le saladier ' . $fruit . " et touillez<br>";
}

