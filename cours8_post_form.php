<?PHP
    session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
</head>
<!-- 
    Formulaire de commande, qui ne contient aucun PHP, c'est l'action qui redirige vers le fichier de traitement
-->
<body>
    <?php
        if(isset($_SESSION['username'])) {
            echo "Bienvenue " . $_SESSION['username'];
            // redirige vers un fichier php, qui va tuer la session et 
            // me rediriger sur cette page
            echo "<a href='cours9_session_stop.php'>Se déconnecter</a>";
        } else {
            echo "Bonjour";
        }
    ?>


    <div class="container">
        <div class="row">
            <form action="cours8_post_action.php" method="post">
                <label for="qte_caput">Caputccino
                    <input type="number" class="form-control" name="qte_caput" id="qte_caput">
                </label>
                <label for="qte_ristr">Ristretto
                    <input type="number" class="form-control" name="qte_ristr" id="qte_ristr">
                </label>
                <label for="is_rgpd">Accord d'utilisation de vos données
                    <input type="checkbox" name="is_rgpd" value='true' class="form-control" id="is_rgpd">
                </label>
                <label for="is_rgpd">Accord d'utilisation de vos données
                    <input type="color" name="color"class="form-control" >
                    <input type="date" name="date" id="">
                    <input type="time" name="heure" id="">
                    <input type="datetime" name="heure" id="">
                </label>
                <input type="submit" value="Commander">
            </form>
        </div>
    </div>
</body>

</html>