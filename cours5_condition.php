<?php

/* *********************** */
/* **** condition *  ***** */
/* *********************** */

// if 

$prix = 10;

if ($prix <= 30) {
    echo "C'est pas cher";
} else if ($prix <= 50) {
    echo "c'est pas donnée";
} else {
    if ($prix) { // si prix est pas nul ou pas à 0 (false)
        echo "c'est pas mal déjà ";
    }
    echo "pas d'avis en particulier";
}

// conbinaison && => ET
if ($prix && $prix > 50) {
    echo "c'est cher";
}

// || OU 
if ($prix || $prix > 50) {
    echo "c'est cher";
}

// SWITCH
$page = '404';

switch ($page) {
    case 'contact':
        // j'affiche la page contact
        break;
    case 'home':
    case 'accueil':
        // j'affiche la page home
        break;
    default:
        # si aucune des possibilités match, alors je passe dans le default
        break;
}
