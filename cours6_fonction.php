<?php

/**
 * Calcul l'aire d'une surface
 * @param integer $largeur
 * @param integer $longueur
 * @return integer
 */
function calcAire($largeur=10, $longueur=500){
    // $largeur=10;
    // $longueur=20;

    return $largeur * $longueur;
}

function showCalcAire($largeur, $longueur){
    // $largeur=10;
    // $longueur=20;

    echo "<h2>" . calcAire($largeur, $longueur) . "</h2>";
}

echo showCalcAire(10, 20);

echo calcAire();