<?php 

    if(isset($_POST) && isset($_POST['test'])){
        $res = $_POST['test'];
    } else {
        $res = "coucou";
    }

    echo $res;
?>

<!-- formulaire de base -->
<form action="#" method="post">
    <input type="text" name="test" placeholder="test">
    <input type="submit" value="envoyer!!">
</form>

<!-- formulaire upload image -->
<form action="cours10_proceed.php" enctype="multipart/form-data" method="POST">
    <!-- limite côté navigateur -->
    <input type="hidden" name="MAX_FILE_SIZE" value="300000">
    <!-- accept affiche par défault que les types selectionnés
    /!\ ne bloque en rien la selection -->
    <input type="file" name="image" accept=".png, image/jpeg" id="">

    <input type="submit" value="envoyer l'image">
</form>
