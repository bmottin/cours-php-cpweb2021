<?php

// integer
$myInt = 42;

// float, real, double
$myFloat = 39.5;

// string
$myStr = "C'est un string";

$myStr2 = 'C\'est un deuxieme string';

// concataner (.), combine 2 chaine de caractere
echo $myStr . " " . $myStr2;
echo "<br>";
// interpolation
echo "Yop $myStr"; // marche mais déconseillé
echo 'Yop $myStr'; // marche pas
echo "<br>";
// booleen ou boolean bool
$myBool = true; // false, 0, 1

// Constante, comme des variables mais on ne peut pas changer leurs valeurs 
const MA_CONSTANTE = "locahost";
define('PORT', '3306');

// commentaire simple ligne

/*
 Commentaire multiligne
*/

/**
 * commentaire autodoc
 * va s'afficher dans l'éditeur de code pour documenter une fonction par exemple
 */

/* *********************************** */
/* ********** opérations ************* */
/* *********************************** */

// operateur de base
// + - * / %

// incrémentation
$i = 42;
$i++; // 43
$i += 1; // 44
$i = $i + 1; // 45

// décrémentation
$i--; // 44
$i -= 3; // 41

/* *********************************** */
/* ********** Manip STR ************* */
/* *********************************** */
echo "<hr>";
$myStrBis = "Coucou";

echo $myStrBis[5];
echo $myStrBis[-1];
echo "<br>";
echo substr($myStrBis, 3, 3); // affiche cou
echo "<pre>";

echo count($myStrBis); // ne marche pas utilisé strlen à la place

$str2 = "Hello World";
var_dump(str_split($str2)); // découpe à toutes les lettres
var_dump(explode(" ", $str2)); // découpe à chaque espace

echo strrev("coucou"); // affiche uocuoc
